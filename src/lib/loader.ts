"use client"
export default function cloudinaryLoader({ src }: { src: string }) {
  return `https://res.cloudinary.com/dhq9acwqr/image/upload/${src}`;
}
