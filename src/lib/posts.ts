import fs from "fs";
import path from "path";
import matter from "gray-matter";

import { remark } from 'remark';
import html from 'remark-html';

const postsDirectory = path.join(process.cwd(), "./src/posts");

console.log(postsDirectory)

export interface PostData {
  id: string;
  title: string;
  date: string;
  // Agrega otras propiedades si es necesario
}

export function getSortedPostsData() {
  const fileNames = fs.readdirSync(postsDirectory);
  const allPostsData = fileNames.map((fileName) => {
    const id = fileName.replace(/\.md$/, "");
    const fullPath = path.join(postsDirectory, fileName);
    const fileContents = fs.readFileSync(fullPath, "utf8");
    const matterResult = matter(fileContents);

    // Asegúrate de que TypeScript conozca el tipo de datos esperado
    const postData = {
      id,
      ...matterResult.data, // Puedes utilizar 'as' para decirle a TypeScript el tipo de matterResult.data
    };
    return postData as PostData;
  });

  return allPostsData.sort((a, b) => {
    if (a.date < b.date) {
      return 1;
    } else {
      return -1;
    }
  });
}

export function getAllPostIds() {
  const fileNames = fs.readdirSync(postsDirectory);

  return fileNames.map((fileName) => {
    return {
      params: {
        id: fileName.replace(/\.md$/, ""),
      },
    };
  });
}

export function getPostData(id: string) {
  const fullPath = path.join(postsDirectory, `${id}.md`);
  const fileContent = fs.readFileSync(fullPath, "utf-8");

  const result = matter(fileContent);

  return {
    id,
    ...result.data,
  };
}
