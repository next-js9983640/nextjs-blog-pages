import React from 'react'
import Link from 'next/link'
import { Url } from 'next/dist/shared/lib/router/router'
import Image from 'next/image'
import cloudinaryLoader from '@/lib/loader'
export function CardLink({ content, icon, path, url }: { content: string, icon?: React.ReactNode, path: Url, url: string }) {
    return (
        <div>

            <Link
                href={path}
                className="overflow-hidden flex flex-col justify-center items-center group rounded-lg border border-transparent transition-colors hover:border-gray-300 hover:bg-gray-100 border-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30 hover:shadow-xl hover:shadow-indigo-500/50"
            >
                <Image
                    loader={cloudinaryLoader}
                    src={url}
                    width={200}
                    height={400}
                    alt='your name'
                    className='h-full w-full'
                />
                <h2 className={`mb-3 text-2xl font-semibold flex-1`}>
                    {content}{' '}
                    {icon && <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
                        {icon}
                    </span>}
                </h2>
                <p className={`m-0 max-w-[30ch] text-sm opacity-50`}>

                </p>
            </Link>
        </div>
    )
}

