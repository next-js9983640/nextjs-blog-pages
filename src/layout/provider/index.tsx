import React from 'react'

function Provider({ children }: { children: React.ReactNode }) {
    return (
        <div className='flex min-h-screen flex-col items-center justify-between p-24'>
            <div className="mb-32 grid place-items-center text-center lg:max-w-6xl gap-10 lg:w-full lg:mb-0 lg:grid-cols-4 lg:text-left">{children}</div>
        </div>
    )
}

export default Provider