import Head from 'next/head';
import Image from 'next/image';

import Link from 'next/link';
import cloudinaryLoader from '@/lib/loader'
import utilStyles from '@/styles/utils.module.css';
import styles from './layout.module.css';
const name = 'Kevin rodriguez';
export const siteTitle = 'Next.js Sample Website';

export default function Cotainer({ children, home }: { children: React.ReactNode, home?: boolean }) {
    return (
        <>
            <div className='flex flex-col justify-center items-center rounded-lg border transition-colors px-3 py-5 border-gray-300 bg-gray-100 dark:border-neutral-700 ' >
                <div className={styles.header}>
                    {home ? (
                        <>
                            <Image
                                loader={cloudinaryLoader}
                                priority
                                src="v1691598451/nft/Default_Maximaliset_Rococo_Astral_Escape_Splash_art_portrait_o_1_e2e0b95e-0ee5-4cc0-a9ad-d6c6728c59da_1_qpf9sr.jpg"
                                className={utilStyles.borderCircle}
                                height={144}
                                width={144}
                                alt=""
                            />
                            <h1 className={utilStyles.heading2Xl}>{name}</h1>
                        </>
                    ) : (
                        <>
                            <Link href="/">
                                <Image
                                    loader={cloudinaryLoader}
                                    priority
                                    src="v1691598454/nft/Default_hyperrealistic_powerful_whitearmored_female_warrior_h_0_9d60cece-3be8-4ec9-aece-5560bdd5b8a0_1_qt2n7m.jpg"
                                    className={utilStyles.borderCircle}
                                    height={108}
                                    width={108}
                                    alt=""
                                />
                            </Link>
                            <h2 className={utilStyles.headingLg}>
                                <Link href="/" className={utilStyles.colorInherit}>
                                    {name}
                                </Link>
                            </h2>
                        </>
                    )}
                </div>
                <div>{children}</div>
                {!home && (
                    <div className={styles.backToHome}>
                        <Link
                            href="/"
                        >
                            <h2 className={`mb-3 text-2xl font-semibold flex-1`}>
                                {"← Back to home"}{' '}
                            </h2>
                        </Link>
                    </div>
                )}
            </div>

        </>
    );
}