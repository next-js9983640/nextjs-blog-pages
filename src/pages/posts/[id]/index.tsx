import React from 'react'
import { GetStaticPaths, GetStaticProps } from 'next'
import { getAllPostIds, getPostData, PostData } from '@/lib/posts'
import Layout from '@/layout/Container'

export const getStaticPaths: GetStaticPaths = async () => {
    const paths = getAllPostIds();
    return {
        paths,
        fallback: false,
    };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
    const postData = await getPostData(params?.id as string);
    return {
        props: {
            postData,
        },
    };
};

function Post({ postData }: { postData: PostData }) {
    return (
        <Layout>
            {postData.title}
            <br />
            {postData.id}
            <br />
            {postData.date}
        </Layout>
    )
}

export default Post