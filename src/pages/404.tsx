import Link from 'next/link'
import { Found } from '@/icons/notFound'
export default function Custom404() {
    return (
        <div className='flex flex-col justify-center items-center gap-5 h-screen'>
            <Found />
            <h2>Not Found</h2>
            <p>Could not find requested resource</p>
            <Link className='text-white no-underline' href="/"><button className='bg-sky-600 px-3 py-2  rounded-xl'>Return Home</button></Link>
        </div>
    )
}