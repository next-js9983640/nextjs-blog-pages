import React from 'react'
import { CardLink } from '@/components/CardLink'
import Provider from '@/layout/provider'
export default function Task() {
    return (
        <Provider>
            <CardLink url='v1691598440/nft/Default_megapolis_cyberpunk_lighting_bar_single_girl_with_head_1_19f3a4d7-9993-44f6-b48e-71f67c289e02_1_lvtpfa.jpg' path={"/"} content='← back to home' />
        </Provider>
    )
}