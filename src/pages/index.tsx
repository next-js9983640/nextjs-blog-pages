import Image from 'next/image'
import { CardLink } from '@/components/CardLink'
import { Inter } from 'next/font/google'
import Layout from '@/layout/Container'
import utilStyles from '@/styles/utils.module.css';
import { getSortedPostsData, PostData } from '@/lib/posts'

export async function getStaticProps() {
  const allPostsData = getSortedPostsData()
  return {
    props: {
      allPostsData
    }
  }
}

export default function Home({ allPostsData }: { allPostsData: PostData[] }) {
  return (
    <main
      className={`flex min-h-screen flex-col items-center justify-between p-24`}
    >
      <div className="mb-32 grid place-items-center text-center lg:max-w-5xl gap-10 lg:w-full lg:mb-0 lg:grid-cols-4 lg:text-left">
        <CardLink url='v1691602431/nft/Default_a_full_body_picture_of_a_pretty_high_school_teen_wear_1_ddd272d5-62a6-43c3-bfb0-db00e85a81c1_1_h29xne.jpg' path={"/posts"} content='Posts' icon={<span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
          -&gt;
        </span>} />
        <CardLink url='v1691598451/nft/Default_Maximaliset_Rococo_Astral_Escape_Splash_art_portrait_o_1_e2e0b95e-0ee5-4cc0-a9ad-d6c6728c59da_1_qpf9sr.jpg' path={"/task"} content='Tasks' icon={<span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
          -&gt;
        </span>} />
        <CardLink url='v1691602446/nft/Default_35mm_F28_insanely_hyperdetailed_and_intricate_realisti_4_8ded548d-2341-46a5-90c6-8a42582b43da_1_wxjvlx.jpg' path={"/profile"} content='Profile' icon={<span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
          -&gt;
        </span>} />
      </div>
      <Layout home>
        <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
          <h2 className={utilStyles.headingLg}>Blog</h2>
          <ul className={utilStyles.list}>
            {allPostsData.map(({ id, date, title }) => (
              <li className={utilStyles.listItem} key={id}>
                {title}
                <br />
                {id}
                <br />
                {date}
              </li>
            ))}
          </ul>
        </section>
      </Layout>

    </main>
  )
}
